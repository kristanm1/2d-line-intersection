import matplotlib.pyplot as plt


def abs(x):
    return -x if x < 0 else x


def p(A, B, t):
    x = A[0] + t*(B[0]-A[0])
    y = A[1] + t*(B[1]-A[1])
    return x, y


def intersection(A, B, C, D, tol=1e-9):
    # find and return intersection point or segment of two lines:
    # p(t) = A+t(B-A)
    # p(s) = C+s(D-C)
    a1, a2 = B[0]-A[0], C[0]-D[0]
    a3, a4 = B[1]-A[1], C[1]-D[1]
    det = a1*a4-a3*a2
    if abs(det) < tol:
        # only using information from p(t) = A+t(B-A) line segment
        # more precise solution would use q(s) = C+s(D-C) line segment too*?!?
        tc, td = None, None
        if abs(a1) >= tol and abs(a3) >= tol:
            tc1, tc2 = (C[0]-A[0])/a1, (C[1]-A[1])/a3
            td1, td2 = (D[0]-A[0])/a1, (D[1]-A[1])/a3
            if abs(tc1-tc2) < tol: tc = (tc1+tc2)/2
            if abs(td1-td2) < tol: td = (td1+td2)/2
        elif abs(a1) < tol and abs(a3) >= tol:
            if abs(A[0]-C[0]) < tol: tc = (C[1]-A[1])/a3
            if abs(A[0]-D[0]) < tol: td = (D[1]-A[1])/a3
        elif abs(a1) >= tol and abs(a3) < tol:
            if abs(A[1]-C[1]) < tol: tc = (C[0]-A[0])/a1
            if abs(A[1]-D[1]) < tol: td = (D[0]-A[0])/a1
        else: return None
        if abs(tc) < tol or abs(tc-1) < tol:
            return "Point", p(A, B, tc)
        if abs(td) < tol or abs(td-1) < tol:
            return "Point", p(A, B, td)
        if 0 < tc < 1:
            if td > 1: return "Segment", (p(A, B, tc), B)
            if td < 0: return "Segment", (p(A, B, tc), A)
        if 0 < td < 1:
            if tc > 1: return "Segment", (p(A, B, td), B)
            if tc < 0: return "Segment", (p(A, B, td), A)
    else:
        ia1, ia2 = a4/det, -a2/det
        ia3, ia4 = -a3/det, a1/det
        b1, b2 = C[0]-A[0], C[1]-A[1]
        t, s = ia1*b1 + ia2*b2, ia3*b1 + ia4*b2
        if 0 <= t <= 1 and 0 <= s <= 1:
            P1, P2 = p(A, B, t), p(C, D, s)
            return "Point", ((P1[0]+P2[0])/2, (P1[1]+P2[1])/2)
    return None


A, B, C, D = (-2, 1), (1, -3), (-3, -4), (2, 7)
A, B, C, D = (0, 0), (2, 2), (1, 1), (3, 3)
A, B, C, D = (0, 0), (1, 1), (2, 2), (3, 3)

plt.plot((A[0], B[0]), (A[1], B[1]), "r-")
plt.plot((C[0], D[0]), (C[1], D[1]), "r-")
plt.plot((A[0], B[0], C[0], D[0]), (A[1], B[1], C[1], D[1]), "ro")

X = intersection(A, B, C, D)
if X is not None:
    intersectionType = X[0]
    if intersectionType == "Point":
        plt.plot(X[1][0], X[1][1], "bo")
    elif intersectionType == "Segment":
        plt.plot((X[1][0][0], X[1][1][0]), (X[1][0][1], X[1][1][1]), "b-")
        plt.plot((X[1][0][0], X[1][1][0]), (X[1][0][1], X[1][1][1]), "bo")

plt.show()
    
